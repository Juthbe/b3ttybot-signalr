# Define base image
FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build-env

# Copy project files
WORKDIR /source

COPY ["b3ttybot-service/b3ttybot-service.csproj", "./b3ttybot-service/b3ttybot-service.csproj"]

# Restore
RUN dotnet restore "./b3ttybot-service/b3ttybot-service.csproj"

# Copy all source code
COPY . .

# Publish
WORKDIR /source/b3ttybot-service
RUN dotnet publish -c Release -o /publish

# Runtime
FROM  mcr.microsoft.com/dotnet/aspnet:7.0
WORKDIR /publish
COPY --from=build-env /publish .
ENTRYPOINT ["dotnet", "b3ttybot-service.dll"]
