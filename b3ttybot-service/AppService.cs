﻿using b3ttybot_service.Services.BotService;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace b3ttybot_service
{
  internal class AppService : BackgroundService
  {
    private ILogger<AppService> Logger { get; }
    private IBotService BotService { get; }

    public AppService(ILogger<AppService> logger, IBotService botService)
    {
      this.Logger = logger;
      this.BotService = botService;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
      Logger.LogInformation("AppService running. Starting Bot");

      _ = this.BotService.InitAsync();      

      while (!stoppingToken.IsCancellationRequested)
      {
        await Task.Delay(1000, stoppingToken);
      }
    }

  }
}
