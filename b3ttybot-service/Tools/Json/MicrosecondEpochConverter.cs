﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace b3ttybot_service.Tools.Json
{
  public class MicrosecondEpochConverter : DateTimeConverterBase
  {
    private static readonly DateTime _epoch = new(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

    public override void WriteJson(JsonWriter writer, object? value, JsonSerializer serializer)
    {
      if (value != null)
        writer.WriteRawValue(value.ToString());
    }

    public override object? ReadJson(JsonReader reader, Type objectType, object? existingValue, JsonSerializer serializer)
    {
      if (reader.Value == null) { return null; }
      return _epoch.AddMilliseconds((long)reader.Value);
    }
  }
}
