﻿using Serilog;
using Serilog.Sinks.SystemConsole.Themes;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using b3ttybot_service.Services.Configuration;
using b3ttybot_service.Services.BotService;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog.Core;
using b3ttybot_service.Services.Signal;
using System;
using b3ttybot_service.Services.Gpt;

namespace b3ttybot_service
{
  public class Program
  {
    public static async Task Main(string[] args)
    {
      Log.Logger = SetupDefaultLogger("a");
      await CreateHostBuilder(args).Build().RunAsync();
    }

    private static IHostBuilder CreateHostBuilder(string[] args)
    {
      return Host.CreateDefaultBuilder(args)
        .ConfigureLogging((logging) =>
        {
          logging.ClearProviders();
          logging.AddSerilog(Log.Logger);
        })
        .ConfigureServices(services =>
        {
          services.AddSingleton<IBotConfiguration, BotConfiguration>();
          services.AddSingleton<IBotService, BotService>();
          services.AddSingleton<ISignalApiService, SignalApiService>();
          services.AddSingleton<IChatGptService, ChatGptService>();
          services.AddHostedService<AppService>();
        });
    }

    private static Serilog.ILogger SetupDefaultLogger(string name)
    {
      var template = "[{Timestamp:yyyy.MM.dd HH:mm:ss.fff}] {Level:u3} " + name + " {SourceContext}: {Message:lj}{NewLine}{Exception}";

      var loggerConfig = new LoggerConfiguration()
        .MinimumLevel.Debug()
        .Enrich.FromLogContext()
        .WriteTo.Console(outputTemplate: template, theme: AnsiConsoleTheme.Literate);

      var logPath = Environment.GetEnvironmentVariable("logPath");
      if (logPath != null)
      {
        loggerConfig.WriteTo.File(logPath, outputTemplate: template);
      }

      return loggerConfig.CreateLogger();      
    }
  }
}
