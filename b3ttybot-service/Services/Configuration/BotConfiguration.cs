﻿using b3ttybot_service.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace b3ttybot_service.Services.Configuration
{
  public class BotConfiguration : IBotConfiguration
  {
    private ILogger<BotConfiguration> Logger { get; }

    public BotConfiguration(ILogger<BotConfiguration> logger)
    {
      this.Logger = logger;

      try
      {
        this.Account = Environment.GetEnvironmentVariable("account") ?? throw new Exception("\"account\" environment variable not set");
        this.Host = Environment.GetEnvironmentVariable("host") ?? throw new Exception("\"host\" environment variable not set");        
        var portString = Environment.GetEnvironmentVariable("port") ?? throw new Exception("\"Port\" environment variable not set");
        this.Port = int.Parse(portString);
        this.GPTApiKey = Environment.GetEnvironmentVariable("gpt_key") ?? throw new Exception("\"gpt_key\" environment variable not set");
        this.WhitelistPath = Environment.GetEnvironmentVariable("whitelistPath") ?? throw new Exception("\"whitelistPath\" environment variable not set");
        this.Whitelist = JsonConvert.DeserializeObject<WhitelistModel>(File.ReadAllText(WhitelistPath)) ?? throw new Exception("whitelist parsing failed");
      }
      catch (Exception ex)
      {
        this.Logger.LogCritical("Configuration Error: {ex}", ex);
        Environment.Exit(-1);
      }
    }

    public string Account { get; private set; }
    public string Host { get; private set; }
    public int Port { get; private set; }
    public string ApiBaseUrl => $"http://{Host}:{Port}";
    public string GPTApiKey { get; private set; }
    public string WhitelistPath { get; private set; }
    public WhitelistModel Whitelist { get; private set; }
  }
}
