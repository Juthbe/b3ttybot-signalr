﻿using b3ttybot_service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace b3ttybot_service.Services.Configuration
{
  public interface IBotConfiguration
  {
    public string Account { get; }
    public string Host { get; }
    public int Port { get; }
    public string ApiBaseUrl { get; }
    public string GPTApiKey { get; }
    public string WhitelistPath { get; }
    public WhitelistModel Whitelist { get; }
  }
}
