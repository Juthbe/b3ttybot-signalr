﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace b3ttybot_service.Services.BotService
{
  public interface IBotService
  {
    Task InitAsync();
  }
}
