﻿using b3ttybot_service.Areas;
using b3ttybot_service.Areas.Base;
using b3ttybot_service.Models;
using b3ttybot_service.Models.SignalApiResponses;
using b3ttybot_service.Services.Configuration;
using b3ttybot_service.Services.Gpt;
using b3ttybot_service.Services.Signal;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Websocket.Client;

namespace b3ttybot_service.Services.BotService
{
  public class BotService : IBotService
  {
    private ILogger<BotService> Logger { get; }
    private IBotConfiguration Config { get; }
    private ISignalApiService SignalApi { get; }
    private IChatGptService GptService { get; }

    private List<SignalGetGroupResponse> SignalGroups { get; set; } = new List<SignalGetGroupResponse>();

    private static List<AreaBase> Areas = new List<AreaBase>();

    public BotService(ILogger<BotService> logger, IBotConfiguration config, ISignalApiService signalApi, IChatGptService gptService)
    {
      this.Logger = logger;
      this.Config = config;
      this.SignalApi = signalApi;
      this.GptService = gptService;

      this.Logger.LogDebug("BotService constructed");
    }

    public async Task InitAsync()
    {
      this.Logger.LogInformation("BotService init...");

      Areas.Add(new CountdownArea(this.SignalApi));
      Areas.Add(new RedditContentArea(this.SignalApi));

      await this.GetGroups();
      await this.StartReceiving();

      this.Logger.LogInformation("BotService init done");
    }

    private async Task GetGroups()
    {
      this.SignalGroups = await this.SignalApi.GetGroupsAsync();
      this.Logger.LogInformation("Groups received");
    }

    private async Task StartReceiving()
    {
      var websocketEndpoint = new Uri($"ws://{this.Config.Host}:{this.Config.Port}/v1/receive/{this.Config.Account}");

      var client = new WebsocketClient(websocketEndpoint);

      client.ReconnectTimeout = null;
      client.ReconnectionHappened.Subscribe(info => Logger.LogInformation($"Reconnection happened, type: {info.Type}"));
      client.DisconnectionHappened.Subscribe(info =>
      {
        Logger.LogError($"Disconnection happened, type: {info}");
      });

      client.MessageReceived.Subscribe(msg =>
      {
        try
        {
          this.OnMessageReceived(msg);
        }
        catch (Exception ex)
        {
          this.Logger.LogError("Error in OnMessageReceived. Error: {ex}", ex);
        }
      });

      await client.Start();
    }

    private void OnMessageReceived(ResponseMessage msg)
    {
      if (msg.MessageType == System.Net.WebSockets.WebSocketMessageType.Text)
      {
        var obj = JsonConvert.DeserializeObject<dynamic>(msg.Text);
        Logger.LogDebug($"Message received: {JsonConvert.SerializeObject(obj, Formatting.Indented)}");

        var receivedMessage = JsonConvert.DeserializeObject<ReceivedMessageModel>(msg.Text);

        if (receivedMessage == null)
        {
          Logger.LogError($"Received Message was null or could ne deseriliazed. Received raw json was {msg.Text}");
          return;
        }

        //Logger.LogInformation($"Message received: {JsonConvert.SerializeObject(receivedMessage, Formatting.Indented)}");
        //Logger.LogInformation($"Message received: {receivedMessage.Envelope.Timestamp}");

        if (string.IsNullOrWhiteSpace(receivedMessage.Envelope.DataMessage?.Message) == false)
        {
          if (receivedMessage.Envelope.DataMessage.Message.StartsWith("/"))
          {
            HandleCommand(receivedMessage.Envelope);
          }
          else if (receivedMessage.Envelope.DataMessage.Mentions != null)
          {
            if (receivedMessage.Envelope.DataMessage.Mentions.Any(m => m.Number == Config.Account))
            {
              HandleGpt(receivedMessage.Envelope, true);
            }
          }
          else
          {
            HandleGpt(receivedMessage.Envelope, false);
          }
        }
      }
    }

    private void HandleCommand(Envelope env)
    {
      var command = env.DataMessage?.Message;
      var recipients = new List<string>();

      if (string.IsNullOrWhiteSpace(command))
        return;

      if (env.DataMessage?.GroupInfo == null)
      {
        if (!Config.Whitelist.IsValidDirect(env.SourceUuid))
        {
          Logger.LogWarning($"User \"{env.SourceUuid}\" not whitelisted.");
          return;
        }

        recipients.Add(env.SourceUuid);
      }
      else
      {
        var group = this.SignalGroups.Where(g => g.Internal_id == env.DataMessage?.GroupInfo.GroupId).FirstOrDefault();
        if (group == null)
        {
          Logger.LogWarning($"Received a groupmessage from a group that is unknown. Skipping message.");
          return;
        }

        if (!Config.Whitelist.IsValidGroup(group.Id))
        {
          Logger.LogWarning($"Group \"{group.Id}\" not whitelisted.");
          return;
        }

        recipients.Add(group.Id);
      }

      if (command.ToLowerInvariant() == "/help" || command.ToLowerInvariant() == "/usage")
      {
        SendUsages(recipients, env);
        return;
      }
      else if(command.ToLowerInvariant() == "/reset")
      {

        if (Config.Whitelist.IsValidAdmin(env.SourceUuid))
        {
          this.GptService.Reset();
          this.SignalApi.SendText(recipients, "zurückgesetzt");
        }
        else
        {
          this.SignalApi.SendText(recipients, "nope");
        }
      }
      else
      {
        try
        {
          MatchRoutes(command, recipients, env);
        }
        catch (Exception ex)
        {
          Logger.LogError(ex, "Uncatched Route Processing Error");
        }
      }
    }

    private void HandleGpt(Envelope env, bool mention)
    {
      var recipients = new List<string>();

      if (env.DataMessage?.GroupInfo == null)
      {
        if (!Config.Whitelist.IsValidDirect(env.SourceUuid))
        {
          Logger.LogWarning($"User \"{env.SourceUuid}\" not whitelisted.");
          return;
        }

        recipients.Add(env.SourceUuid);
      }
      else
      {
        var group = this.SignalGroups.Where(g => g.Internal_id == env.DataMessage?.GroupInfo.GroupId).FirstOrDefault();
        if (group == null)
        {
          Logger.LogWarning($"Received a groupmessage from a group that is unknown. Skipping message.");
          return;
        }

        if (!Config.Whitelist.IsValidGroup(group.Id))
        {
          Logger.LogWarning($"Group \"{group.Id}\" not whitelisted.");
          return;
        }

        recipients.Add(group.Id);
      }

      if (mention)
      {
        this.SignalApi.SetTyping(recipients);
      }

      if (string.IsNullOrWhiteSpace(env.DataMessage?.Message) == false)
      {
        if(env.DataMessage.Message.Length > 1500)
        {
          this.SignalApi.HideTyping(recipients);
          this.SignalApi.SendText(recipients, $"Zuviel Text ({env.DataMessage.Message.Length} Zeichen)...wird ignoriert.");
          return;
        }

        if (env.DataMessage.Message == "￼")
        {
          this.GptService.AppendUserInput(env.SourceUuid, env.SourceName, "Antworte mir bitte.");
        }
        else
        {
          this.GptService.AppendUserInput(env.SourceUuid, env.SourceName, env.DataMessage?.Message);
        }
      }

      if (mention)
      {
        var response = this.GptService.GetResponseFromChatbotAsync().GetAwaiter().GetResult();
        this.SignalApi.HideTyping(recipients);
        this.SignalApi.SendText(recipients, response);
      }
    }

    private async void SendUsages(List<string> recipients, Envelope request)
    {
      StringBuilder sb = new StringBuilder();
      sb.AppendLine("Usage");
      foreach (var area in Areas)
      {
        sb.AppendLine("");

        foreach (var route in area.Routes)
          sb.AppendLine($"{route.Example}");
      }

      await this.SignalApi.SendText(recipients, sb.ToString());
    }

    private async void MatchRoutes(string cmd, List<string> recipients, Envelope msg)
    {
      foreach (var area in Areas)
      {
        foreach (var route in area.Routes)
        {
          if (route.Process(cmd, recipients, msg) == true)
          {
            Logger.LogInformation("Hit. Matched Route with pattern: {0} . Text was: {1}", route.Pattern, cmd);
            return;
          }
        }
      }
    }
  }
}
