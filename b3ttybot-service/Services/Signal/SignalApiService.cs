﻿using b3ttybot_service.Models.SignalApiResponses;
using b3ttybot_service.Services.Configuration;
using Microsoft.Extensions.Logging;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace b3ttybot_service.Services.Signal
{
  public class SignalApiService : ISignalApiService
  {
    private IBotConfiguration Config { get; }
    private ILogger<SignalApiService> Logger { get; }

    public SignalApiService(ILogger<SignalApiService> logger, IBotConfiguration config)
    {
      this.Logger = logger;
      this.Config = config;

      this.Logger.LogDebug("SignalApiService constructed");
    }

    public async Task<List<SignalGetGroupResponse>> GetGroupsAsync()
    {
      RestClient rclient = new(this.Config.ApiBaseUrl);
      RestRequest req = new($"/v1/groups/{this.Config.Account}");
      var result = await rclient.ExecuteGetAsync<List<SignalGetGroupResponse>>(req).ConfigureAwait(false);

      if (result.StatusCode != System.Net.HttpStatusCode.OK)
      {
        this.Logger.LogError("Get groups failed with status {code}. Error {error}", result.StatusCode, result.ErrorMessage);
      }

      if (result.Data == null)
      {
        this.Logger.LogError("Get groups failed. Data was null.");
        return new List<SignalGetGroupResponse>();
      }

      return result.Data;
    }

    public async Task SendText(List<string> recipients, string message)
    {
      RestRequest req = new("/v2/send");
      req.AddJsonBody(new
      {
        message = message,
        number = this.Config.Account,
        recipients = recipients,
      });

      RestClient rclient = new(this.Config.ApiBaseUrl);
      var result = await rclient.ExecutePostAsync(req);
    }

    public async Task SendMedia(List<string> recipients, string base64File)
    {
      RestRequest req = new("/v2/send");
      req.AddJsonBody(new
      {
        base64_attachments = new string[] { base64File },
        number = this.Config.Account,
        recipients = recipients,
      });

      RestClient rclient = new(this.Config.ApiBaseUrl);
      var result = await rclient.ExecutePostAsync(req);
    }

    public async Task SetTyping(List<string> recipients)
    {
      RestRequest req = new($"/v1/typing-indicator/{this.Config.Account}");
      req.AddJsonBody(new
      {
        recipient = recipients.First()
      });

      RestClient rclient = new(this.Config.ApiBaseUrl);
      var result = await rclient.ExecutePutAsync(req);
    }

    public async Task HideTyping(List<string> recipients)
    {
      RestRequest req = new($"/v1/typing-indicator/{this.Config.Account}");
      req.AddJsonBody(new
      {
        recipient = recipients.First()
      });
      req.Method = Method.Delete;

      RestClient rclient = new(this.Config.ApiBaseUrl);
      var result = await rclient.ExecuteAsync(req);
    }
  }
}
