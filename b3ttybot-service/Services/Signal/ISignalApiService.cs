﻿using b3ttybot_service.Models.SignalApiResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace b3ttybot_service.Services.Signal
{
  public interface ISignalApiService
  {
    public Task<List<SignalGetGroupResponse>> GetGroupsAsync();
    public Task SendText(List<string> recipients, string message);
    public Task SendMedia(List<string> recipients, string base64File);

    public Task SetTyping(List<string> recipients);
    public Task HideTyping(List<string> recipients);
  }
}
