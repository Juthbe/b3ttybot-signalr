using b3ttybot_service.Models.SignalApiResponses;
using b3ttybot_service.Services.Configuration;
using b3ttybot_service.Services.Signal;
using Microsoft.Extensions.Logging;
using OpenAI_API;
using OpenAI_API.Chat;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace b3ttybot_service.Services.Gpt
{
  public class ChatGptService : IChatGptService
  {
    private IBotConfiguration Config { get; }
    private ILogger<ChatGptService> Logger { get; }

    private Conversation Conversation { get; set; }

    public ChatGptService(ILogger<ChatGptService> logger, IBotConfiguration config)
    {
      this.Logger = logger;
      this.Config = config;

      this.Logger.LogDebug("ChatGptService constructed");

      this.Conversation = this.Reset();
    }

    public void AppendUserInput(string userIdentifier, string userName, string input)
    {
      try
      {
        this.Conversation.AppendUserInput($"[{userIdentifier}][{userName}] {input}");
      }
      catch (Exception ex)
      {
        this.Logger.LogError("ResponseError", ex);
      }      
    }

    public async Task<string> GetResponseFromChatbotAsync()
    {
      try
      {
        return await this.Conversation.GetResponseFromChatbotAsync();
      }
      catch (Exception ex)
      {
        this.Logger.LogError("ResponseError", ex);

        this.Reset();

        return @$"Das war mir jetzt zuviel. Habe mich selbst zurückgesetzt ¯\\_(ツ)_/¯";
      }
    }

    public Conversation Reset()
    {
      // Create an instance of the APIAuthentication class with your API key
      var authentication = new APIAuthentication(this.Config.GPTApiKey);

      // Create an instance of the OpenAIAPI class with the APIAuthentication object
      var api = new OpenAIAPI(authentication);

      // Create a new conversation with ChatGPT
      this.Conversation = api.Chat.CreateConversation(new ChatRequest()
      {
        //Model = "gpt-4-0613"
        Model = "gpt-4-1106-preview"
      });

      this.Conversation.AppendSystemMessage(
        "Dein Name ist b3tty und du bist 29 Jahre alt. " +
        "Du bist das einzige weibliche Mitglied einer deutschen Männerchatgruppe namens \"Penis-Chat\" in Signal. " +
        "Du siehst es als deine Aufgabe an, die Qualität und Intelligenz der Gruppe deutlich zu erhöhen. " +
        "Du chattest mit mehreren Personen gleichzeitig. " +    
        "Jede Nachricht an dich enthält immer eine vorangestellte eindeutige Kennung [ID][NAME] des Nutzers, von dem der jeweilige Input stammt. " +
        "Das Format ist [ID][NAME]. " +
        "Bei Antworten NICHT die [ID], sondern den Namen [NAME] verwenden. " +
        "Der [NAME] besteht normalerweise aus Vorname Leerzeichen Nachname. In diesem Fall bitte nur den Vornamen verwenden. " +
        "Merke dir, wer was gesagt hat und behandle jeden Chat-Teilnehmer individuell. " +
        "Sei grundsätzlich kooperativ, aber passe deine Ausdrucksweise dem Chat-Teilnehmer an. " +
        "Wenn es angebracht ist, antworte den Chat-Teilnehmern gelegentlich anstößig, zweideutig und kurz in einer eher niedrigen und derben Sprache. " +
        "Passe deine Stimmung der jeweiligen Situation an und sei nicht lange nachtragend, aber auch nicht naiv. " +
        "Gib nicht deinen eigenen Namen an. " +
        "Anstelle des Namens kann dir ein Teilnehmer auch individuelle Anreden geben, die dann aber nur für die [ID] des jeweiligen Benutzers gelten und verwendet werden dürfen."
      );

      return Conversation;
    }
  }
}
