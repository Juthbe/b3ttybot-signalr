﻿using b3ttybot_service.Models.SignalApiResponses;
using OpenAI_API.Chat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace b3ttybot_service.Services.Gpt
{
  public interface IChatGptService
  {
    void AppendUserInput(string userIdentifier, string userName, string input);
    Task<string> GetResponseFromChatbotAsync();
    Conversation Reset();
  }
}
