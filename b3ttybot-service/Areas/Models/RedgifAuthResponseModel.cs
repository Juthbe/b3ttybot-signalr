﻿using System;
using System.Collections.Generic;
using System.Text;

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
namespace b3ttybot_service.Areas.Models
{
  public class RedgifAuthResponseModel
  {

    public string Token { get; set; }
    public string Addr { get; set; }
    public string Agent { get; set; }
    public string Rtfm { get; set; }

  }
}
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
