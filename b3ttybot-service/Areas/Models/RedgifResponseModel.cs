﻿using System;
using System.Collections.Generic;
using System.Text;

namespace b3ttybot_service.Areas.Models
{
  public class RedgifResponseModel
  {
    public Gif Gif { get; set; }
  }

  public class Gif
  {
    public string Id { get; set; }
    public Urls Urls { get; set; }
  }

  public class Urls
  {
    public string Sd { get; set; }
    public string Hd { get; set; }
    public string Poster { get; set; }
    public string Thumbnail { get; set; }
    public string Vthumbnail { get; set; }
  }
}
