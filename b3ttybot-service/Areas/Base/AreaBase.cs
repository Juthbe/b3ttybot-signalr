﻿using b3ttybot_service.Models;
using b3ttybot_service.Services.Signal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace b3ttybot_service.Areas.Base
{
  public abstract class AreaBase
  {
    public AreaBase(ISignalApiService signalApi)
    {
      this.SignalApi = signalApi;
    }

    public ISignalApiService SignalApi { private set; get; }

    public List<Route> Routes { get; set; } = new List<Route>();
  }

  public class Route
  {
    public Route(string pattern, string example, Action<Match, string, List<string>, Envelope> action)
    {
      this.Pattern = pattern;
      this.Example = example;
      this.Action = action;
      this.Regex = new Regex(this.Pattern);
    }

    public string Pattern { get; }
    public string Example { get; }
    public Action<Match, string, List<string>, Envelope> Action { get; }
    private Regex Regex { get; }

    public bool Process(string cmd, List<string> recipients, Envelope envelope)
    {
      var match = Regex.Match(cmd);
      if (match.Success)
      {
        this.Action(match, cmd, recipients, envelope);
        return true;
      }

      return false;
    }

  }

}
