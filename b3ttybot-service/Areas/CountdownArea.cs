﻿using b3ttybot_service.Areas.Base;
using b3ttybot_service.Models;
using b3ttybot_service.Services.Signal;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace b3ttybot_service.Areas
{
  public class CountdownArea : AreaBase
  {
    public CountdownArea(ISignalApiService signalApi)
      : base(signalApi)
    {
      this.Logger = Log.Logger.ForContext<CountdownArea>();

      this.Routes.Add(new Route(@"^\/cd (?<event>[a-z_]+)$", "/cd weinfest\n/cd cabd", (match, cmd, recipients, msg) => SendCountDown(cmd, recipients, msg, Mode.Daily, match.Groups["event"].Value.ToLowerInvariant(), "")));
      this.Routes.Add(new Route(@"^\/cd (?<event>[a-z_]+) (?<year>[^$]+)$", "/cd weinfest 2030\n/cd cabd 2030", (match, cmd, recipients, msg) => SendCountDown(cmd, recipients, msg, Mode.Daily, match.Groups["event"].Value.ToLowerInvariant(), match.Groups["year"].Value)));
    }

    private ILogger Logger { get; }
    private enum Mode { Daily, Random };

    private async void SendCountDown(string cmd, List<string> recipients, Envelope env, Mode m, string eventNameProvided, string yearProvided)
    {
      this.Logger.Information("{eventName} {year}", eventNameProvided, yearProvided);

      var validEvents = new List<string>() { "cabd", "weinfest" };

      int yearParsed = DateTime.Now.Year;

      if (yearProvided != string.Empty && int.TryParse(yearProvided, out yearParsed) == false)
      {
        await this.SignalApi.SendText(recipients, "#invalidyear");
        return;
      }

      if (yearParsed < 1)
      {
        await this.SignalApi.SendText(recipients, "#invalidyear");
        return;
      }

      if (!validEvents.Contains(eventNameProvided))
      {
        await this.SignalApi.SendText(recipients, "#invalidevent");
        return;
      }

      switch (eventNameProvided)
      {
        case "cabd":
          {
            DateTime cabdDate = new DateTime();

            if (yearParsed != 0)
            {
              cabdDate = new DateTime(yearParsed, 12, 16, 18, 0, 0, 0);
            }
            else
            {
              TimeSpan dateDiff = DateTime.Now - new DateTime(2200, 12, 31, 00, 0, 0, 0);
              int yeari = DateTime.Now.Year - 1;

              DateTime nowDate = DateTime.Now;

              while (dateDiff.TotalSeconds < 0)
              {
                yeari += 1;
                cabdDate = new DateTime(yeari, 12, 26, 18, 0, 0, 0);
                dateDiff = cabdDate - nowDate;
              }
            }

            await this.SignalApi.SendText(recipients, GetFormatedCountDown(cabdDate, "*Frohlocket!*", "\n\nbis zur *CABD* *" + cabdDate.Year + "*"));

            return;
          }

        case "weinfest":
          {
            DateTime wfDate = new DateTime();

            int yeari = 0;
            if (yearParsed != 0)
            {
              yeari = yearParsed;
            }
            else
            {
              yeari = DateTime.Now.Year;
            }

            yeari = yeari - 1;
            int month = 8;
            TimeSpan dateDiff = DateTime.Now - new DateTime(2200, 12, 31, 00, 0, 0, 0);

            while (dateDiff.TotalSeconds < 0)
            {
              yeari += 1;

              DateTime thirdFriday = new DateTime(yeari, month, 15, 18, 0, 0, 0);
              while (thirdFriday.DayOfWeek != DayOfWeek.Friday)
              {
                thirdFriday = thirdFriday.AddDays(1);
              }

              wfDate = thirdFriday;
              dateDiff = wfDate - DateTime.Now;
            }

            await this.SignalApi.SendText(recipients, GetFormatedCountDown(wfDate, string.Empty, "\n\nbis zum *Weinfest* *" + wfDate.Year + "*"));
            return;
          }
      }
    }

    public string GetFormatedCountDown(DateTime eventDate, string prefix = "", string suffix = "")
    {
      DateTime nowDate = DateTime.Now;
      TimeSpan dateDiff = eventDate - nowDate;

      long days = Math.DivRem(Convert.ToInt64(dateDiff.TotalSeconds), 3600 * 24, out long remainder);
      long hours = Math.DivRem(remainder, 3600, out remainder);
      long minutes = Math.DivRem(remainder, 60, out long seconds);

      string result = $"{prefix}\nnoch\n{days} Tage\n{hours} Stunden\n{minutes} Minuten\n{seconds} Sekunden {suffix}.";

      return result;
    }
  }
}
