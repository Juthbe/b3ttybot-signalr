﻿using b3ttybot_service.Areas.Base;
using RestSharp.Authenticators.OAuth2;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using b3ttybot_service.Services.Signal;
using b3ttybot_service.Models;
using b3ttybot_service.Areas.Models;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace b3ttybot_service.Areas
{
  public class RedditContentArea : AreaBase
  {
    public RedditContentArea(ISignalApiService signalApi)
      : base(signalApi)
    {
      this.Logger = Log.Logger.ForContext<RedditContentArea>();

      this.Routes.Add(new Route(@"^\/random (?<sub>[a-zA-Z0-9_]+)$", "/random tits", (match, cmd, recipients, msg) => SendRedditPictureFailSafe(cmd, recipients, msg, Mode.Random, match.Groups["sub"].Value)));
      this.Routes.Add(new Route(@"^\/risky$", "/risky", (match, cmd, recipients, msg) => SendRedditPictureFailSafe(cmd, recipients, msg, Mode.Random, "randnsfw")));

      this.Routes.Add(new Route(@"^\/daily (?<sub>[a-zA-Z0-9_]+)$", "/daily tits", (match, cmd, recipients, msg) => SendRedditPictureFailSafe(cmd, recipients, msg, Mode.Daily, match.Groups["sub"].Value)));
      this.Routes.Add(new Route(@"^\/weekly (?<sub>[a-zA-Z0-9_]+)$", "/weekly tits", (match, cmd, recipients, msg) => SendRedditPictureFailSafe(cmd, recipients, msg, Mode.Weekly, match.Groups["sub"].Value)));
      this.Routes.Add(new Route(@"^\/monthly (?<sub>[a-zA-Z0-9_]+)$", "/monthly tits", (match, cmd, recipients, msg) => SendRedditPictureFailSafe(cmd, recipients, msg, Mode.Monthly, match.Groups["sub"].Value)));
      this.Routes.Add(new Route(@"^\/yearly (?<sub>[a-zA-Z0-9_]+)$", "/yearly tits", (match, cmd, recipients, msg) => SendRedditPictureFailSafe(cmd, recipients, msg, Mode.Yearly, match.Groups["sub"].Value)));
      this.Routes.Add(new Route(@"^\/alltime (?<sub>[a-zA-Z0-9_]+)$", "/alltime tits", (match, cmd, recipients, msg) => SendRedditPictureFailSafe(cmd, recipients, msg, Mode.Alltime, match.Groups["sub"].Value)));
    }

    private ILogger Logger { get; }
    private enum Mode { Alltime, Yearly, Monthly, Weekly, Daily, Random };

    private string redgifsBearerToken = string.Empty;

    private async void SendRedditPictureFailSafe(string cmd, List<string> recipients, Envelope env, Mode m, string sub)
    {
      await this.SignalApi.SetTyping(recipients);

      var homo_blacklist = new List<string> { "dilf", "gay", "penis", "trap", "homo" };
      var nope_blacklist = new List<string> { "gilf" };

      if (homo_blacklist.Contains(sub.ToLowerInvariant()))
      {
        await this.SignalApi.SendText(recipients, "#nohomo");
        return;
      }

      if (nope_blacklist.Contains(sub.ToLowerInvariant()))
      {
        await this.SignalApi.SendText(recipients, "#nope");
        return;
      }

      int retry = 0;

      while (retry < 10)
      {
        try
        {
          if (await SendRedditPictureInternal(cmd, recipients, env, m, sub))
          {
            return;
          }
        }
        catch (Exception ex)
        {
          this.Logger.Error("Exception while SendRedditPictureInternal. Details: {ex}", ex);
          await this.SignalApi.SendText(recipients, "#leidernein (E2)");

          return;
        }
        finally
        {
          await this.SignalApi.HideTyping(recipients);
        }

        retry++;
        this.Logger.Debug("Retry: {retry}", retry);
        Thread.Sleep(50);
      }

      await this.SignalApi.SendText(recipients, "#leidernein (E1)");
    }

    private async Task<bool> SendRedditPictureInternal(string cmd, List<string> recipients, Envelope env, Mode m, string sub)
    {
      var restClient = new RestClient();

      RestRequest request = null;

      switch (m)
      {
        case Mode.Random:
          request = new RestRequest($"https://www.reddit.com/r/{sub}/random.json");
          break;
        case Mode.Alltime:
          request = new RestRequest($"https://www.reddit.com/r/{sub}/top.json?t=all&limit=1");
          break;
        case Mode.Yearly:
          request = new RestRequest($"https://www.reddit.com/r/{sub}/top.json?t=year&limit=1");
          break;
        case Mode.Monthly:
          request = new RestRequest($"https://www.reddit.com/r/{sub}/top.json?t=month&limit=1");
          break;
        case Mode.Weekly:
          request = new RestRequest($"https://www.reddit.com/r/{sub}/top.json?t=week&limit=1");
          break;
        case Mode.Daily:
          request = new RestRequest($"https://www.reddit.com/r/{sub}/top.json?t=day&limit=1");
          break;

        default:
          throw new InvalidOperationException($"Unknown mode {m}");
      }

      var response = await restClient.ExecuteAsync(request).ConfigureAwait(false);
      var jsonDoc = JsonDocument.Parse(response.Content);

      JsonElement element = new JsonElement();
      if (jsonDoc.RootElement.ValueKind == JsonValueKind.Array)
      {
        element = jsonDoc.RootElement[0];
      }
      else if (jsonDoc.RootElement.ValueKind == JsonValueKind.Object)
      {
        element = jsonDoc.RootElement;
      }
      else
      {
        throw new InvalidOperationException($"Received structur is of type {jsonDoc.RootElement.ValueKind}");
      }

      var item = element.GetProperty("data").GetProperty("children")[0].GetProperty("data");

      if (m == Mode.Random)
      {
        var count = element.GetProperty("data").GetProperty("children").GetArrayLength();
        var randindex = new Random().Next(0, count - 1);
        item = element.GetProperty("data").GetProperty("children")[randindex].GetProperty("data");
      }

      var id = item.GetProperty("id").GetString();
      var originalUrl = item.GetProperty("url").GetString();
      var title = item.GetProperty("title").GetString();
      var domain = item.GetProperty("domain").GetString();
      var subreddit = item.GetProperty("subreddit").GetString();

      var url = originalUrl;

      try
      {

        if (domain == "v.redd.it")
        {
          //Message sentMessage = await Program.Bot.SendVideoAsync(chatId: msg.Chat.Id, video: url); //nope
          Logger.Information("cant: {url}", url);
          return false;
        }
        else if (domain == "i.redd.it" && (url.EndsWith(".jpg") || url.EndsWith(".png")))
        {
          RestClient imageClient = new RestClient();
          RestRequest imageRequest = new RestRequest($"{url}");

          using (MemoryStream ms = new MemoryStream())
          {
            imageRequest.ResponseWriter = (responseStream) =>
            {
              using (responseStream)
              {
                responseStream.CopyTo(ms);
                return responseStream; //New 2023
              }
            };

            var result = imageClient.DownloadData(imageRequest);

            ms.Seek(0, SeekOrigin.Begin);
            var data = Convert.ToBase64String(ms.ToArray());

            await this.SignalApi.SendMedia(recipients, data);
            return true;
          }
        }
        else if (domain == "gfycat.com")
        {
          {
            var clientId = Environment.GetEnvironmentVariable("gfycat_client_id");
            var clientSecret = Environment.GetEnvironmentVariable("gfycat_client_id");

            RestClient gfyClient = new RestClient();
            RestRequest gfyRequest = new RestRequest("https://api.gfycat.com/v1/oauth/token", Method.Post);
            gfyRequest.AddJsonBody(new { grant_type = "client_credentials", client_id = clientId, client_secret = clientSecret });
            var gfyTokenResponse = gfyClient.Execute(gfyRequest);
            var gfyTokenJsonDoc = JsonDocument.Parse(gfyTokenResponse.Content);
            var token = gfyTokenJsonDoc.RootElement.GetProperty("access_token").GetString();
            var gfyIdUrl = $"https://api.gfycat.com/v1/gfycats/{url.Split("/").Last().Split("-").First()}";
            gfyRequest = new RestRequest(gfyIdUrl, Method.Get);
            gfyRequest.AddHeader("Authorization", $"Bearer {token}");
            var gfyResponse = gfyClient.Execute(gfyRequest);

            if (gfyResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
              var gfyJson = JsonDocument.Parse(gfyResponse.Content);
              //url = gfyJson.RootElement.GetProperty("gfyItem").GetProperty("gifUrl").GetString();
              url = gfyJson.RootElement.GetProperty("gfyItem").GetProperty("mp4Url").GetString();

              //TODO:
              //Message sentMessage = await Program.SendVideoAsync(chatId: msg.Chat.Id, video: url, caption: sub == "randnsfw" ? subreddit : null);
              return true;
            }
            else
            {
              return false;
            }
          }
        }
        else if (domain == "i.imgur.com" && url.EndsWith(".gifv"))
        {
          RestClient imgurClient = new RestClient();

          RestRequest imgurRequest = new RestRequest($"https://i.imgur.com/{url.Split("/").Last()}".Replace("gifv", "mp4"));

          using (MemoryStream ms = new MemoryStream())
          {
            imgurRequest.ResponseWriter = (responseStream) =>
            {
              using (responseStream)
              {
                responseStream.CopyTo(ms);
                return responseStream; //New 2023
              }
            };

            var result = imgurClient.DownloadData(imgurRequest);

            ms.Seek(0, SeekOrigin.Begin);
            var data = Convert.ToBase64String(ms.ToArray());

            await this.SignalApi.SendMedia(recipients, data);
            //Message sentMessage = await Program.Bot.SendVideoAsync(chatId: msg.Chat.Id, video: ms, caption: sub == "randnsfw" ? subreddit : null);
            return true;
          }
        }
        else if (domain == "redgifs.com")
        {
          RestClient redgifClient = new RestClient();
          //redgifClient.UserAgent = "PostmanRuntime/7.26.8";
          redgifClient.AddDefaultHeader("User-Agent", "PostmanRuntime/7.26.8");

          var downloadUrl = string.Empty;

          for (int i = 0; i < 3; i++)
          {
            if (string.IsNullOrEmpty(this.redgifsBearerToken))
            {
              RestRequest authRequest = new RestRequest($"https://api.redgifs.com/v2/auth/temporary", Method.Get);
              var authResponse = await redgifClient.ExecuteAsync<RedgifAuthResponseModel>(authRequest);
              this.redgifsBearerToken = authResponse.Data.Token;
            }

            RestRequest metaDataRequest = new RestRequest($"https://api.redgifs.com/v2/gifs/{url.Split("/").Last()}", Method.Get);


            var options = new RestClientOptions();
            options.Authenticator = new OAuth2AuthorizationRequestHeaderAuthenticator(redgifsBearerToken, "Bearer");
            redgifClient = new RestClient(options);
            redgifClient.AddDefaultHeader("User-Agent", "PostmanRuntime/7.26.8");


            var metaDataResponse = await redgifClient.ExecuteAsync<RedgifResponseModel>(metaDataRequest);

            if (metaDataResponse.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
              this.redgifsBearerToken = string.Empty;
            }
            else
            {
              downloadUrl = metaDataResponse.Data.Gif.Urls.Hd;
              break;
            }
          }

          RestRequest videoRequest = new RestRequest($"{downloadUrl}");

          using (MemoryStream ms = new MemoryStream())
          {
            videoRequest.ResponseWriter = (responseStream) =>
            {
              using (responseStream)
              {
                responseStream.CopyTo(ms);
                return responseStream;
              }
            };

            var result = redgifClient.DownloadData(videoRequest);

            ms.Seek(0, SeekOrigin.Begin);

            var data = Convert.ToBase64String(ms.ToArray());
            await this.SignalApi.SendMedia(recipients, data);

            //Message sentMessage = await Program.Bot.SendVideoAsync(chatId: msg.Chat.Id, video: ms, caption: sub == "randnsfw" ? subreddit : null);
            return true;
          }
        }

        var extension = Path.GetExtension(url).ToLowerInvariant(); ;

        switch (extension)
        {
          case ".gif":
            {
              RestClient imgurClient = new RestClient();

              RestRequest imgurRequest = new RestRequest(url);

              using (MemoryStream ms = new MemoryStream())
              {
                imgurRequest.ResponseWriter = (responseStream) =>
                {
                  using (responseStream)
                  {
                    responseStream.CopyTo(ms);
                    return responseStream; //New 2023
                  }
                };

                var result = imgurClient.DownloadData(imgurRequest);

                ms.Seek(0, SeekOrigin.Begin);
                var data = Convert.ToBase64String(ms.ToArray());

                await this.SignalApi.SendMedia(recipients, data);

                return true;
              }
            }

          case ".jpg":
            {
              RestClient imgurClient = new RestClient();

              RestRequest imgurRequest = new RestRequest(url);

              using (MemoryStream ms = new MemoryStream())
              {
                imgurRequest.ResponseWriter = (responseStream) =>
                {
                  using (responseStream)
                  {
                    responseStream.CopyTo(ms);
                    return responseStream; //New 2023
                  }
                };

                var result = imgurClient.DownloadData(imgurRequest);

                ms.Seek(0, SeekOrigin.Begin);
                var data = Convert.ToBase64String(ms.ToArray());

                await this.SignalApi.SendMedia(recipients, data);

                return true;
              }
            }

          default:
            {
              //Message sentMessage = await Program.Bot.SendPhotoAsync(chatId: msg.Chat, photo: url, caption: sub == "randnsfw" ? subreddit : null);
              await this.SignalApi.SendText(recipients, $"TODO (Domain: {domain} | Extension: {extension} )");
              return true;
            }
        }
      }
      catch (Exception ex)
      {
        this.Logger.Error(ex, "Failed for {url} => original was {originalUrl}", url, originalUrl);
        return false;
      }
    }
  }
}
