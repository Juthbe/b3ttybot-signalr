﻿using b3ttybot_service.Tools.Json;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

namespace b3ttybot_service.Models
{
  public class ReceivedMessageModel
  {
    public Envelope Envelope { get; set; }
    public string Account { get; set; }
    public int Subscription { get; set; }
  }

  public class Envelope
  {
    public string Source { get; set; }
    public string SourceNumber { get; set; }
    public string SourceUuid { get; set; }
    public string SourceName { get; set; }
    public int SourceDevice { get; set; }

    [JsonConverter(typeof(MicrosecondEpochConverter))]
    public DateTime Timestamp { get; set; }
    public DataMessage? DataMessage { get; set; }
  }

  public class DataMessage
  {
    [JsonConverter(typeof(MicrosecondEpochConverter))]
    public DateTime Timestamp { get; set; }
    public string Message { get; set; }
    public int ExpiresInSeconds { get; set; }
    public bool ViewOnce { get; set; }
    public GroupInfo GroupInfo { get; set; }

    public List<MentionInfo>? Mentions { get; set; }

  }

  public class MentionInfo
  {
    public string Name { get; set; }
    public string Number { get; set; }
    public string Uuid { get; set; }    
  }

  public class GroupInfo
  {
    public string GroupId { get; set; }
    public string Type { get; set; }
  }
}

#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

