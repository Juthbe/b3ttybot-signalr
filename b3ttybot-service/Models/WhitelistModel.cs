﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace b3ttybot_service.Models
{
  public class WhitelistModel
  {
    public List<string> AllowedGroupIDs { get; set; } = new List<string>();
    public List<string> AllowedDirectIDs { get; set; } = new List<string>();
    public List<string> AdminIDs { get; set; } = new List<string>();

    public bool IsValidGroup(string test)
    {
      return this.AllowedGroupIDs.Contains(test);
    }

    public bool IsValidDirect(string test)
    {
      return this.AllowedDirectIDs.Contains(test);
    }

    public bool IsValidAdmin(string test)
    {
      return this.AdminIDs.Contains(test);
    }
  }
}
