﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
namespace b3ttybot_service.Models.SignalApiResponses
{
  public class SignalGetGroupResponse
  {
    public string[] Admins { get; set; }
    public bool Blocked { get; set; }
    public string Id { get; set; }
    public string Internal_id { get; set; }
    public string Invite_link { get; set; }
    public string[] Members { get; set; }
    public string Name { get; set; }
    public string[] Pending_Invites { get; set; }
    public string[] Pending_Requests { get; set; }
  }
}
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
